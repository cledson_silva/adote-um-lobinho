//Aqui ficam declarações e funções que várias páginas podem usufruir
//const document
const $ = document;
//função manipulação do header
let header_container_links = $.querySelector('.nav-container');
window.addEventListener('scroll', () => {
	if (window.pageYOffset > 0) {
		header_container_links.classList.add('link-active');
	} else {
		header_container_links.classList.remove('link-active');
	}
});
//função de criar o card do lobo
const createWolfCard = (wolf) => {
	let wolfCard = $.createElement('div');
	wolfCard.innerHTML = `
	<div class="container-descricao-lobinhos">
		<div class="container-foto">
			<img class="foto-lobo" src="${wolf.link_image}" alt="Foto de um lobinho">
		</div>
		<div>
			<div class="container-texto">
				<div>
					<h3 class="nome-lobo" >${wolf.name}</h3>
					<p class="idade-lobo" >Idade: ${wolf.age} ano${wolf.age > 1 ? 's' : ''}</p>
				</div>
					<button class="button-quem-somos-nos" onclick="window.location.href = 'wolf.html?id=${wolf.id}'">Adotar</button>
			</div>
			<p class="descricao-lobo">${wolf.description}</p>
		</div>

`;
	wolfCard.classList.add();
	return wolfCard;
};
