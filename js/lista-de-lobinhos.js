const url = 'https://lobinhos.herokuapp.com/wolves/';
const $ = document;
function adicionaNomeLobo(lista_nome) {
	let nome1 = $.getElementById('nome-1');
	let nome2 = $.getElementById('nome-2');
	let nome3 = $.getElementById('nome-3');
	let nome4 = $.getElementById('nome-4');

	nome1.innerHTML = lista_nome[0];
	nome2.innerHTML = lista_nome[1];
	nome3.innerHTML = lista_nome[2];
	nome4.innerHTML = lista_nome[3];
}
function adicionarIdadeLobo(lista_idade) {
	let idade1 = $.getElementById('idade-1');
	let idade2 = $.getElementById('idade-2');
	let idade3 = $.getElementById('idade-3');
	let idade4 = $.getElementById('idade-4');
	idade1.innerHTML = 'Idade: ' + lista_idade[0] + ' anos';
	idade2.innerHTML = 'Idade: ' + lista_idade[1] + ' anos';
	idade3.innerHTML = 'Idade: ' + lista_idade[2] + ' anos';
	idade4.innerHTML = 'Idade: ' + lista_idade[3] + ' anos';
}
function adicionaDescricao(lista_descricao) {
	let descricao1 = $.getElementById('descricao-1');
	let descricao2 = $.getElementById('descricao-2');
	let descricao3 = $.getElementById('descricao-3');
	let descricao4 = $.getElementById('descricao-4');
	descricao1.innerHTML = lista_descricao[0];
	descricao2.innerHTML = lista_descricao[1];
	descricao3.innerHTML = lista_descricao[2];
	descricao4.innerHTML = lista_descricao[3];
}
function adicionaFotoLobinho(link) {
	let foto1 = $.getElementById('foto-1');
	let foto2 = $.getElementById('foto-2');
	let foto3 = $.getElementById('foto-3');
	let foto4 = $.getElementById('foto-4');
	foto1.src = link[0];
	foto2.src = link[1];
	foto3.src = link[2];
	foto4.src = link[3];
}
let lista_de_ids = [];
fetch(url)
	.then((data) => data.json())
	.then((resp) => {
		console.log(resp[0]);
		let links_fotos = [];
		let lista_nome = [];
		let lista_idade = [];
		let lista_descricao = [];
		for (let i = 0; i < 5; i++) {
			links_fotos.push(resp[i].link_image);
			lista_nome.push(resp[i].name);
			lista_idade.push(resp[i].age);
			lista_descricao.push(resp[i].description);
			lista_de_ids.push(resp[i].id);
		}

		adicionaNomeLobo(lista_nome);
		adicionarIdadeLobo(lista_idade);
		adicionaDescricao(lista_descricao);
		adicionaFotoLobinho(links_fotos);
	})
	.catch((err) => console.log(err));

let passaValor = function(valor) {
	window.location = 'adotar_lobinho.html?id=' + valor;
};

let botao1 = $.getElementById('adotar-1');
botao1.addEventListener('click', (event) => {
	event.preventDefault();
	passaValor(lista_de_ids[0]);
});
let botao2 = $.getElementById('adotar-2');
botao2.addEventListener('click', (event) => {
	event.preventDefault();
	passaValor(lista_de_ids[1]);
});
let botao3 = $.getElementById('adotar-3');
botao3.addEventListener('click', (event) => {
	event.preventDefault();
	passaValor(lista_de_ids[2]);
});
let botao4 = $.getElementById('adotar-4');
botao4.addEventListener('click', (event) => {
	event.preventDefault();
	passaValor(lista_de_ids[3]);
});
