const url = 'https://lobinhos.herokuapp.com/wolves/';
const $ = document;
function queryString(parameter) {
	let loc = location.search.substring(1, location.search.length);
	let param_value = false;
	let params = loc.split('&');
	for (let i = 0; i < params.length; i++) {
		param_name = params[i].substring(0, params[i].indexOf('='));

		if (param_name == parameter) {
			param_value = params[i].substring(params[i].indexOf('=') + 1);
		}
	}
	if (param_value) {
		return param_value;
	} else {
		return undefined;
	}
}

let id_lobinho = queryString('id');

fetch(url + id_lobinho)
	.then((data) => data.json())
	.then((resp) => {
		console.log(resp);
		let foto_lobo = $.getElementById('foto-lobo');
		let nome_lobo = $.getElementById('nome-lobo');
		let id_lobo = $.getElementById('id-lobo');
		foto_lobo.src = resp.link_image;
		nome_lobo.innerHTML = 'Adote o(a): ' + resp.name;
		id_lobo.innerHTML = 'ID: ' + resp.id;
	})
	.catch((err) => {
		console.log(err);
	});
