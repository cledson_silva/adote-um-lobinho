//Aqui ficam as funçoes especificas da pagina principal
let wolves_container = $.querySelector('.list-lobinho-container');
getWolves().then((wolves) => {
	let min = 1;
	let max = wolves.length;
	let random1 = Math.floor(Math.random() * (max + min));
	let random2 = Math.floor(Math.random() * (max + min));

	while (random1 === random2) {
		random2 = Math.floor(Math.random() + (max - min)) + min;
	}
	let card1 = createWolfCard(wolves[random1]);
	let card2 = createWolfCard(wolves[random2]);
	card1.classList.add('card-lobos');
	card2.classList.add('card-lobos');
	card1.querySelector('button').remove();
	card2.querySelector('button').remove();
	wolves_container.append(card1);
	wolves_container.append(card2);
});
